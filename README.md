# Informações #

Olá a todos.

eu desenvolvi esse tema para meu trabalho diário, porque gosto de um visual mais limpo.

### Utilizei: ###
+ Twitter Boostrap 3
+ Jquery 1.11
+ Select2

Estou utilizando há alguns dias e estou gostando do resultado.

Segue o link do projeto no github para quem quiser baixar.

Poço por gentileza para deixar os meus créditos.

Grande abraço.

---------------------------------------------------------------------------
Hello everyone.

I developed this theme for my daily work, because I like a cleaner look.

I used:
Twitter Bootstrap 3
jquery 1:11
Select2

I'm using a few days ago and I'm liking the result.

Follow the project on github link for those who want to download.

Well please to leave my credits.

Big hug.

### Instalação / Setup ###
cp DarkOrange /usr/local/ispconfig/interface/web/template

